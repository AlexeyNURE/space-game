#ifndef _PLAYER_
#define _PLAYER_

#include <string>

class Player
{
private:

	std::string m_Name;

	int m_Points;

public:

	std::string const & getName() const noexcept
	{
		return m_Name;
	}

	void setNewName(std::string const & _name) noexcept
	{
		if (!_name.empty())
		m_Name = _name;
	}

	void resetPoints() noexcept { m_Points = 0; }

	void increasePoints(int _points = 1) noexcept { m_Points += _points; }

	int getPoints() const { return m_Points; }
};
 
#endif // ! _PLAYER_HPP_
