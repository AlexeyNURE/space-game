#include "rotatbleObject.hpp"


void RotatbleObject::rotateOn(POINT _coords) noexcept
{
	POINT thisCentreCoords{ getCentreCoords() };

	double angle;
	double cathetusAdjasent;
	double cathetusOther;

	if (thisCentreCoords.y < _coords.y) // *this is higher
	{
		cathetusAdjasent = _coords.y - thisCentreCoords.y;
		cathetusOther = abs(_coords.x - thisCentreCoords.x);
		angle = (atan(cathetusOther / cathetusAdjasent) / 3.14) * 180.0;
	}
	else
	{
		cathetusAdjasent = _coords.x - thisCentreCoords.x;
		cathetusOther = abs(thisCentreCoords.y - _coords.y);
		angle = (atan(cathetusOther / cathetusAdjasent) / 3.14) * 180;
		if (thisCentreCoords.x > _coords.x)
			angle *= -1;

		angle += 90;
	}

	if (thisCentreCoords.x < _coords.x)
		angle *= -1;
	setAngle(angle);
}

void RotatbleObject::rotate() noexcept
{
	if (rotate_left)
		m_currentAngle -= m_rotateAngle;

	if (rotate_right)
		m_currentAngle += m_rotateAngle;

	if (m_currentAngle > 360)
		m_currentAngle -= 360;
	else if (m_currentAngle < 0)
		m_currentAngle += 360;
}