#include "enemy.hpp"
#include "Shot.hpp"
#include "Gameplay.hpp"

#pragma comment( lib, "Gdiplus.lib" )

extern Gameplay gameplay;
extern HWND Wnd;

Enemy::Enemy( int _y, SpawnSide _side )
	:	RotatbleObject( 50 )
{
	m_isOnPlace = false;
	m_currentHP = m_maxHP;
	m_lastTimeShoot = 0;
	m_currentAngle = 0;

	m_image = new Gdiplus::Image( L"images/enemy.png" );

	//-------------------------------------------------------

	RECT rt;
	GetClientRect( Wnd, &rt );

	if ( _side == SpawnSide::LEFT )
		m_coords = POINT{ rt.left - m_bitmapSideLenght, _y };
	else
		m_coords = POINT{ rt.right, _y };

	if ( _y > rt.bottom - m_bitmapSideLenght )
		m_coords.y = rt.bottom - m_bitmapSideLenght;
}

Enemy::~Enemy()
{
	delete m_image;
}

Shot * Enemy::createShot()
{
	if ( !m_isOnPlace )
		return nullptr;

	Shot * pShot{ nullptr };
	clock_t currentClock{ clock() };

	if ( currentClock - m_lastTimeShoot >= m_shootDelay )
	{
		pShot = new Shot( getCentreCoords(), ( m_currentAngle + 180 ) % 360, 5 );
		m_lastTimeShoot = currentClock;
	}

	return pShot;
}

void Enemy::update() noexcept
{
	rotate();
	move();
}

void Enemy::rotate() noexcept
{
	POINT shipCentreCoords{ gameplay.getShip()->getCentreCoords() };

	rotateOn(shipCentreCoords);
}

void Enemy::move()
{
	if ( m_isOnPlace )
		return;

	RECT rt;
	GetClientRect( Wnd, &rt );

	if ( m_coords.x <= rt.left )
		m_coords.x += m_moveSpeed;
	else
		m_coords.x -= m_moveSpeed;

	if ( isValidOnScreen( rt ) )
		m_isOnPlace = true;
}
