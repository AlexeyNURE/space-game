#include "Gameplay.hpp"
#include "stage.hpp"
#include "boss.hpp"

#include <cassert>

extern Gameplay gameplay;

POINT Stage::simpleMoves( POINT _coords, RECT _rect, MoveSide & _side, std::size_t _moveSpeed )
{
	POINT newCoords = _coords;

	newCoords.x += _moveSpeed * static_cast< int >( _side );

	if ( !gameplay.getBoss()->isValidOnScreen( _rect ) )
	{
		_side = static_cast< MoveSide >( static_cast< int >( _side ) * -1 );
		assert( _side == MoveSide::RIGHT || _side == MoveSide::LEFT );
		newCoords.x += 2 * _moveSpeed * static_cast< int >( _side );
	}

	return newCoords;
}
