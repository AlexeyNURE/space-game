#pragma once
#include "FileManager.hpp"

class ConfigFile
	:public FileManager
{
public:
	ConfigFile();
	~ConfigFile() override;
	void writeLastName(std::string const &);
	std::string getLastName();
};

