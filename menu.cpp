#include "menu.hpp"
#include <gdiplus.h>
#include "Gameplay.hpp"
#include <string>
#include "SettingsFile.hpp"
#include "ConfigFile.hpp"
using namespace Gdiplus;
extern HWND Wnd, WndMenu;
extern HWND StartButton, QuitButton, StatsButton, BackButton, PlayerButton, EditButton, OKButton, ResetButton;
extern Gameplay gameplay;

Menu::Menu()
	:config(), settings()
{
	isMenu = true;
	isStats = false;
	isPlayerMenu = false;
	gameplay.getPlayer().setNewName(config.getLastName());
}

Menu::~Menu()
{
}

void Menu::draw(HDC hdc)
{
	Graphics graphics(hdc);
	Font font(&FontFamily(L"Arial"), 14, 2);
	SolidBrush brush(Color(255, 255, 255));
	RECT rt;
	GetClientRect(WndMenu, &rt);
	std::string s;

	int x{ ButtonX }, y{ 150 };


	if (isStats)
	{
		showStats();

		settings.open(std::ios_base::out | std::ios_base::in);
		settings.getStream().seekg(0, std::ios_base::beg);

		if (!settings.is_empty())
			while (!settings.getStream().eof())
			{
				EnableWindow(ResetButton, true);
				std::getline(settings.getStream(), s);
				if (s == "Best results:")
				{
					y = 150;
					x = ButtonX + 300;
				}
				graphics.DrawString(std::wstring(s.begin(), s.end()).c_str(), s.size() + 1, &font, PointF(x, y), &brush);
				y += 25;
			}
		else
			EnableWindow(ResetButton, false);

		settings.close();
	}
	if (isMenu)
	{
		showMenu();
	}

	if (isPlayerMenu)
	{
		GetClientRect(WndMenu, &rt);
		showPlayer();
		s = "You play as " + gameplay.getPlayer().getName();
		graphics.DrawString(std::wstring(s.begin(), s.end()).c_str(), s.size() + 1, &font, PointF(ButtonX, 130), &brush);
		s = "Change your name: ";
		graphics.DrawString(std::wstring(s.begin(), s.end()).c_str(), s.size() + 1, &font, PointF(ButtonX, 170), &brush);
	}
}

void Menu::showStats()
{
	ShowWindow(StartButton, SW_HIDE);
	ShowWindow(StatsButton, SW_HIDE);
	ShowWindow(QuitButton, SW_HIDE);
	ShowWindow(PlayerButton, SW_HIDE);
	ShowWindow(ResetButton, SW_SHOW);
	ShowWindow(BackButton, SW_SHOW);
}

void Menu::showMenu()
{
	ShowWindow(StartButton, SW_SHOW);
	ShowWindow(StatsButton, SW_SHOW);
	ShowWindow(QuitButton, SW_SHOW);
	ShowWindow(PlayerButton, SW_SHOW);
	ShowWindow(BackButton, SW_HIDE);
	ShowWindow(OKButton, SW_HIDE);
	ShowWindow(ResetButton, SW_HIDE);
	ShowWindow(EditButton, SW_HIDE);
}

void Menu::showPlayer()
{
	ShowWindow(StartButton, SW_HIDE);
	ShowWindow(StatsButton, SW_HIDE);
	ShowWindow(QuitButton, SW_HIDE);
	ShowWindow(PlayerButton, SW_HIDE);
	ShowWindow(BackButton, SW_SHOW);
	ShowWindow(OKButton, SW_SHOW);
	ShowWindow(EditButton, SW_SHOW);
}

void Menu::press(Buttons _button)
{
	switch (_button)
	{
	case Buttons::Start:
		ShowWindow(WndMenu, SW_HIDE);
		ShowWindow(Wnd, SW_SHOWMAXIMIZED);
		gameplay.initGame();
		break;
	case Buttons::Stats:
		isStats = true;
		isMenu = false;
		InvalidateRect(WndMenu, NULL, TRUE);
		break;
	case Buttons::Quit:
		PostQuitMessage(0);
		break;
	case Buttons::Back:
		isMenu = true;
		isStats = false;
		isPlayerMenu = false;
		InvalidateRect(WndMenu, NULL, TRUE);
		break;
	case Buttons::PlayerMenu:
		isMenu = false;
		isPlayerMenu = true;
		InvalidateRect(WndMenu, NULL, TRUE);
		break;
	case Buttons::OK:
		static char buffer[20];
		GetWindowText(EditButton, buffer, 20);
		gameplay.getPlayer().setNewName(buffer);
		config.writeLastName(buffer);
		InvalidateRect(WndMenu, NULL, TRUE);
		break;
	case Buttons::Reset:
		settings.clearFile();
		InvalidateRect(WndMenu, NULL, TRUE);
		break;
	case Buttons::Amount:
		throw std::logic_error("Wrong button");
		break;
	default:

		break;
	}
}

void Menu::drawBackground(HDC _hdc)
{
	RECT rt;
	GetClientRect(WndMenu, &rt);
	Graphics graphics(_hdc);
	Image background(L"images/mainBackground.jpg");
	//background.RotateFlip(RotateNoneFlipX);
	graphics.DrawImage(&background, 0, 0, rt.right, rt.bottom);
}