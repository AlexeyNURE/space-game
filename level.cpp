#include "level.hpp"
#include "boss.hpp"
#include "stageFactory.hpp"
#include "alienShip.hpp"
#include "monsterShip.hpp"
#include "monsterShipStages.hpp"
#include "alienShipStages.hpp"
#include "DoubleComet.hpp"
#include "SimpleComet.hpp"
#include "enemy.hpp"

#pragma comment( lib, "Gdiplus.lib" )

extern HWND Wnd;

/******************************************************************************/

RECT Level::getPlayerRect() const
{
	RECT res;
	GetClientRect( Wnd, &res );
	res.top = res.bottom / 2;

	return res;
}

template< typename CometType >
CometType * Level::spawnComet( int _minimalSpeed )
{
	RECT rect;
	GetClientRect( Wnd, &rect );

	// when we minimize the window, bottom will be = 0, so 1 check insted of 4
	if ( !rect.bottom )
		return nullptr;

	POINT coords{ 0, 0 };
	int move_speed;
	double k;
	int b;

	int window_lenght = rect.right - rect.left;
	coords.x = rand() % window_lenght;
	move_speed = rand() % 1 + _minimalSpeed;
	b = rand() % 30;
	do
	{
		k = ( double ) ( rand() % 5000 ) / 1000;
	} while ( k < 0.4 );

	if ( coords.x > window_lenght / 2 )
		k *= -1.0;

	return new CometType( coords, k, b, move_speed );
}

/******************************************************************************/

LevelOne::LevelOne()
	: Level( 5 )
{
	Gdiplus::Image * backgroundImage = new Gdiplus::Image( L"images/level1.jpg" );
	backgroundImage->RotateFlip( Gdiplus::RotateNoneFlipX );
	m_brush = new Gdiplus::TextureBrush( backgroundImage );
	delete backgroundImage;
}

Boss * LevelOne::createBoss() const
{
	RECT _rect;
	GetClientRect( Wnd, &_rect );

	POINT spawnPlace;
	spawnPlace.x = ( _rect.right - _rect.left ) / 2;
	spawnPlace.y = 0;

	AlienShip * boss = new AlienShip( spawnPlace );

	return boss;
}

Comet * LevelOne::createComet( int _minimalSpeed ) const
{
	return Level::spawnComet< SimpleComet >( _minimalSpeed );
}

LevelOne::~LevelOne()
{
	delete m_brush;
}

/******************************************************************************/

LevelTwo::LevelTwo()
	: Level( 5 )
{
	Gdiplus::Image * backgroundImage = new Gdiplus::Image( L"images/level2.jpg" );
	backgroundImage->RotateFlip( Gdiplus::RotateNoneFlipY );
	m_brush = new Gdiplus::TextureBrush( backgroundImage );
	delete backgroundImage;
}

Boss * LevelTwo::createBoss() const
{
	RECT _rect;
	GetClientRect(Wnd, &_rect);

	POINT spawnPlace;
	int x = 100 + rand() % _rect.right - 100;
	spawnPlace.x = x;
	spawnPlace.y = 0;

	MonsterShip * boss = new MonsterShip(spawnPlace);

	return boss;
}

Comet * LevelTwo::createComet( int _minimalSpeed ) const
{
	if ( rand() % 2 )
		return Level::spawnComet< SimpleComet >( _minimalSpeed );
	else
		return Level::spawnComet< DoubleComet >( _minimalSpeed );
}

LevelTwo::~LevelTwo()
{
	delete m_brush;
}

/******************************************************************************/

Free::Free()
	: Level(100000)
{
	Gdiplus::Image * backgroundImage = new Gdiplus::Image(L"images/level3.jpg");
	m_brush = new Gdiplus::TextureBrush(backgroundImage);
	delete backgroundImage;
}

Boss * Free::createBoss() const
{
	return nullptr;
}

Comet * Free::createComet(int _minimalSpeed) const
{
	if ( rand() % 2 )
		return Level::spawnComet< SimpleComet >( _minimalSpeed );
	else
		return Level::spawnComet< DoubleComet >( _minimalSpeed );
}

RECT Free::getPlayerRect() const
{
	RECT rt;
	GetClientRect( Wnd, &rt );

	return rt;
}

Enemy * Free::createEnemy() const
{
 	int side = 1 + rand() % 2;
	int y =  50 + rand() %( getPlayerRect().bottom - 50);
	if (side == 1)
	return new Enemy( y, SpawnSide::LEFT );
	else 
	return new Enemy(y, SpawnSide::RIGHT);

}

Free::~Free()
{
	delete m_brush;
}
