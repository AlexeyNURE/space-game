#ifndef _SPACE_SHIP_
#define _SPACE_SHIP_

#include "moveSide.hpp"
#include "FlyingObject.hpp"
#include "rotatbleObject.hpp"

#include <gdiplus.h>
#include <ctime>

class Wave;
class Shot;

class SpaceShip : public RotatbleObject
{
	const static int m_moveSpeed{ 8 };

	const static int m_maxHP{ 100 };

	//---------------------------------------------------

	Gdiplus::Image * m_image;

	Gdiplus::Image * m_imageWhenHit;

	int m_currentHP;

	clock_t m_lastTimeHit;

	//---------------------------------------------------

	void move( RECT _rect ) noexcept;

public:

	SpaceShip();

	~SpaceShip();

	SpaceShip(const SpaceShip &) = delete;

	SpaceShip & operator = (const SpaceShip &) = delete;

	//---------------------------------------------------

	Gdiplus::Image * getImage() noexcept;

	int getCurrentHP() const noexcept;

	int getMaximalHP() const noexcept;

	void setCoordinates( POINT _coords ) noexcept;

	void resetMoving() noexcept;

	//---------------------------------------------------

	Shot * createShot() const noexcept;

	Wave * createWave() const noexcept;

	//---------------------------------------------------

	bool isAlive() const noexcept;

	void update( RECT _rect ) noexcept;

	void takeDamage( int _damage ) noexcept;

	//---------------------------------------------------

	bool move_left, move_right, move_up, move_down;

	const static int m_ShotDamage{ 10 };
};

inline Gdiplus::Image * SpaceShip::getImage() noexcept
{
	if ( clock() - m_lastTimeHit > 1000 )
		return m_image;
	else
		return m_imageWhenHit;
}

inline bool SpaceShip::isAlive() const noexcept
{
	return m_currentHP > 0;
}

inline int SpaceShip::getCurrentHP() const noexcept
{
	return m_currentHP;
}

inline int SpaceShip::getMaximalHP() const noexcept
{
	return m_maxHP;
}

#endif // !_SPACE_SHIP_