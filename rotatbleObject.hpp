#pragma once

#include "FlyingObject.hpp"

class RotatbleObject
	:	public FlyingObject
{
protected:

	const static int m_rotateAngle{ 10 };

	int m_currentAngle;

	RotatbleObject( int _bitmapSideLenght )
		:	FlyingObject( _bitmapSideLenght )
		,	m_currentAngle( 0 )
	{
	};

public:

	int getAngle() const noexcept { return m_currentAngle; }

	virtual void rotate() noexcept;

	void setAngle( int _angle ) noexcept { m_currentAngle = _angle; }

	void resetAngle() noexcept { m_currentAngle = 0; }

	void rotateOn(POINT _coords) noexcept;

	bool rotate_left, rotate_right;

};
