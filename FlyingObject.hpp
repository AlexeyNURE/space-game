#ifndef _FLYING_OBJECT_
#define _FLYING_OBJECT_

#include <Windows.h> 
#include <cmath>
#include "moveSide.hpp"

class FlyingObject
{
protected:

	const int m_bitmapSideLenght;

	POINT m_coords;

	FlyingObject() = delete;

	FlyingObject( int _bitmapSideLenght );

	FlyingObject( const FlyingObject & ) = delete;

public:

	int getXCoord() const noexcept { return m_coords.x; }

	int getYCoord() const noexcept { return m_coords.y; }

	POINT getCoords() const noexcept { return m_coords; }

	int getBitmapSideLenght() const noexcept { return m_bitmapSideLenght; }

	bool isValid( const RECT & _rect ) const noexcept;

	bool isValidOnScreen( const RECT & _rect ) const noexcept;

	POINT getCentreCoords() const noexcept;

	static bool intersects(const FlyingObject & _left, const FlyingObject & _right) noexcept;

	virtual ~FlyingObject() = default;
};

#endif // !_FLYING_OBJECT_