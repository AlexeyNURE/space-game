#ifndef _STAGE_
#define _STAGE_

#include <windows.h>
#include <iostream>
#include <list>

#include "moveSide.hpp"

class Boss;
class Shot;

class Stage
{
protected:

	const std::size_t m_moveSpeed;

	const int m_shootDelay;

	Stage(std::size_t _moveSpeed, int _shootDelay)
		: m_moveSpeed(_moveSpeed), m_shootDelay(_shootDelay)
	{
	}

public:

	virtual POINT move(POINT _coords) = 0;

	virtual std::list< Shot * > shoot(POINT _coords) = 0;

	static POINT simpleMoves( POINT _coords, RECT _rect, MoveSide & _side, std::size_t _moveSpeed );

	virtual bool isComplete() const noexcept = 0;

	virtual ~Stage() = default;
};

#endif // !_STAGE_