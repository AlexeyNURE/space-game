#include "FlyingObject.hpp"
#include "moveSide.hpp"

FlyingObject::FlyingObject( int _bitmapSideLenght )
	: m_bitmapSideLenght( _bitmapSideLenght )
{
}

bool FlyingObject::isValid( const RECT & _rect ) const noexcept
{
	return m_coords.y <= _rect.bottom && m_coords.x >= _rect.left &&
		m_coords.x <= _rect.right && m_coords.y >= _rect.top;
}

bool FlyingObject::isValidOnScreen(const RECT & _rect) const noexcept
{
	if ( m_coords.x < _rect .left || m_coords.x > _rect.right - m_bitmapSideLenght )
		return false;
	if ( m_coords.y < _rect.top || m_coords.y > _rect.bottom - m_bitmapSideLenght )
		return false;

	return true;
}

bool FlyingObject::intersects(const FlyingObject & _left, const FlyingObject & _right) noexcept
{
	POINT leftCentre = _left.getCentreCoords();
	POINT rightCentre = _right.getCentreCoords();

	if ( sqrt(pow(leftCentre.x - rightCentre.x, 2) + pow(leftCentre.y - rightCentre.y, 2)) <
		_left.m_bitmapSideLenght / 2 + _right.m_bitmapSideLenght / 2 )
		return true;

	return false;
}

POINT FlyingObject::getCentreCoords() const noexcept
{
	POINT result;

	result.x = m_coords.x + m_bitmapSideLenght / 2;
	result.y = m_coords.y + m_bitmapSideLenght / 2;

	return result;
}