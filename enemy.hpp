#pragma once

#include "rotatbleObject.hpp"
#include "moveSide.hpp"

#include <gdiplus.h>
#include <ctime>

class Shot;

class Enemy : public RotatbleObject
{
	const static int m_moveSpeed{ 12 };

	const static int m_shootDelay{ 600 };

	const static int m_maxHP{ 30 };

	//--------------------------------------------

	Gdiplus::Image * m_image;

	clock_t m_lastTimeShoot;

	int m_currentHP;

	bool m_isOnPlace;

	//--------------------------------------------

	void rotate() noexcept override;

	void move();

public:

	Enemy( int _y, SpawnSide _side );

	~Enemy();

	//--------------------------------------------

	bool isAlive() const noexcept;

	void takeDamage( int _damage ) noexcept;

	int getCurrentHP() const noexcept;

	int getMaximalHP() const noexcept;

	//--------------------------------------------

	Gdiplus::Image * getImage() noexcept;

	Shot * createShot();

	void update() noexcept;
};

/************************************************/

inline bool Enemy::isAlive() const noexcept
{
	return m_currentHP > 0;
}

inline void Enemy::takeDamage( int _damage ) noexcept
{
	m_currentHP -= _damage;
}

inline int Enemy::getCurrentHP() const noexcept
{
	return m_currentHP;
}

inline int Enemy::getMaximalHP() const noexcept
{
	return m_maxHP;
}

inline Gdiplus::Image * Enemy::getImage() noexcept
{
	return m_image;
}