#include "DoubleComet.hpp"
#include "SimpleComet.hpp"

DoubleComet::DoubleComet(POINT _point, double _k, int _b, int _speed)
	:Comet(_point, _k, _b, _speed, 53)
{
	m_Damage = 10;
}

std::pair <SimpleComet *, SimpleComet *> DoubleComet::destroy()
{
	SimpleComet *first = new SimpleComet(m_coords, m_k + 0.3, m_b, m_speed + 3);
	SimpleComet *second = new SimpleComet(m_coords, -1 * m_k - 0.1, m_b, m_speed + 2);
	return std::make_pair(first, second);
}