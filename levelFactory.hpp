#pragma once

class Level;

class LevelFactory
{
public:

	Level * nextLevel( int _currentLevel ) const;

	~LevelFactory() = default;
};
