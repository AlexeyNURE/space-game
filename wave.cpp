#include "wave.hpp"

Wave::Wave( POINT _coords )
{
	m_centreCoords = _coords;
	m_radius = startRadius;
	m_colorBlue = 255;
}

void Wave::grow() noexcept
{
	m_radius += radiusIncreasing;

	if ( m_radius > maximalRadius - 255 )
		m_colorBlue = maximalRadius - m_radius;
}