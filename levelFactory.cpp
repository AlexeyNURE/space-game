#include "levelFactory.hpp"
#include "level.hpp"

Level * LevelFactory::nextLevel( int _currentLevel ) const
{
	switch ( _currentLevel )
	{
		case 0:
			return new LevelOne;
		case 1:
			return new LevelTwo;
		case 2:
			return new Free;

		default:
			throw std::logic_error( "Invalid stage number" );
	}
	return nullptr;
}
