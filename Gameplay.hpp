#ifndef _GAMEPLAY_
#define _GAMEPLAY_

#define TIMER 1002

#include "SpaceShip.hpp"
#include "Player.hpp"
#include "sounds.hpp"
#include "levelFactory.hpp"

#include <list>
#include <mutex>

#include <gdiplus.h>
using namespace Gdiplus;

class Enemy;
class Comet;
class Shot;
class Wave;
class Level;
class Boss;
class Animation;
class StageFactory;

/**********************************************************************/

class Gameplay
{
	/*------------------------CONTAINING_DATA-------------------------*/

	//--------OBJECTS------------------

	std::list< Shot * > m_shots;

	std::list< Shot * > m_bossShots;

	std::list< Comet * > m_comets;

	std::list< Enemy * > m_enemies;

	SpaceShip * m_ship;

	Boss * m_boss;

	Wave * m_wave;

	Animation * m_bossDestroyAnimation;

	//--------CONTROL------------------

	LevelFactory m_levelFactory;

	Level * m_level;

	int m_currentLevel;

	int m_pointsAfterPreviousLevel;

	Player m_player;

	Sound m_sounds;

	int m_wavesUsed;

	bool m_shipAppears;

	bool m_gameOver;

	bool m_bossSpawnedOnCurrentLevel;

	//--------THREADS------------------

	HANDLE m_moveThread;

	HANDLE m_bossThread;

	//--------FOR_GDI+-----------------

	ULONG_PTR gdiplusToken;

	/*----------------------------MUTEXES-----------------------------*/

	std::mutex m_mutexForComet;

	std::mutex m_mutexForBoss;

	std::mutex m_mutexForShot;

	std::mutex m_mutexForWave;

	std::mutex m_mutexForEnemies;

	std::mutex m_mutexForBossShots;

	/*-----------------------DIFFICULTY_CONTROL-----------------------*/

	const int m_maxAmountShots{ 15 };

	const int m_pointsForSpawnEnemy{ 25 };

	const int m_pointsForGettingWave{ 35 };

	int m_cometMinimalSpeed;

	int m_maxAmountComets;

	void controlDifficulty();

	/*--------------------------CONTROLLERS---------------------------*/

	bool currentLevelComplete() const;

	void loadNextLevel();

	void moveComets();

	void moveWave();

	void moveShots();

	void moveEnemies();

	void checkComets( RECT _rect );

	void checkShots( RECT _rect );

	void checkBossShots( RECT _rect );

	void checkShip();

	void checkEnemies();

	bool isWaveReady() const;

	void bossFightFinished();

	void spawnEmemy();

	void spawnBoss();

	void clearData();

	void clearCometsAndShots();

	RECT chooseShipWay();

	static void move( Gameplay & _gameplay );

	static void controllBoss( Gameplay & _gameplay );

	// returns amount of waves, that you could possible use
	int getPossibleWavesAmount() const;

	/*-----------------------INTERSECTION_CHECK-----------------------*/

	bool objectIntersectsWithAnyPlayerShot( FlyingObject * _obj );

	bool objectIntersectsWithWave( FlyingObject * _obj );

	bool shotIntersectsWithAnyComet( Shot * _pShot );

	bool shipIntersectsWithBossShot();

	/*----------------------------DRAWING-----------------------------*/

	void drawBoss( Graphics & _graphics, HDC _hdc );

	void drawBossHP( HDC _hdc );

	void drawEnemies( Graphics & _graphics );

	void drawShip( Graphics & _graphics, RECT _rect );

	void drawShots( HDC _hdc );

	void drawComets( HDC _hdc);

	void drawWave( HDC _hdc, RECT _rect ) const;

	void drawScore( Graphics & _graphics, HDC _hdc) const;

/**********************************************************************/

public:

	/*-------------------------CTORS_AND_DTOR-------------------------*/

	Gameplay();

	~Gameplay();

	Gameplay(const Gameplay &) = delete;

	Gameplay & operator = (const Gameplay &) = delete;

	/*----------------------------GETTERS-----------------------------*/

	Player & getPlayer();

	SpaceShip * getShip();

	Boss * getBoss() noexcept;

	Sound & getSound();

	/*----------------------------CREATION----------------------------*/

	void spawnWave();

	void spawnComet();

	void spawnShot();

	/*-----------------------------OTHERS-----------------------------*/

	void draw(HDC _hdc);

	void stopGame();

	void initGame();

	void update();
};

/**********************************************************************/

inline Player & Gameplay::getPlayer()
{
	return m_player;
}

inline SpaceShip * Gameplay::getShip()
{
	return m_ship;
}

inline Sound & Gameplay::getSound()
{
	return m_sounds;
}

inline bool Gameplay::currentLevelComplete() const
{
	return m_bossSpawnedOnCurrentLevel && !m_boss;
}

inline Boss * Gameplay::getBoss() noexcept
{
	return m_boss;
}

inline bool Gameplay::isWaveReady() const
{
	return getPossibleWavesAmount() > m_wavesUsed;
}

inline int Gameplay::getPossibleWavesAmount() const
{
	return m_player.getPoints() / m_pointsForGettingWave;
}

#endif // !_GAMEPLAY_
