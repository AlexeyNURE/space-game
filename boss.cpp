#include "boss.hpp"
#include "Shot.hpp"
#include "alienShipStages.hpp"

Boss::Boss(int _maxHP, int _stagesCount, POINT _coords, int _bitmapLenght)
	:	RotatbleObject( _bitmapLenght )
	,	m_maxHP( _maxHP )
	,	m_stagesCount( _stagesCount )
{
	m_currentHP = m_maxHP;
	m_coords = _coords;
	m_coords.x -= m_bitmapSideLenght / 2;
	m_coords.y -= m_bitmapSideLenght;
	m_currentStage = 0;
}

void Boss::checkStage() noexcept
{
	if (m_stage->isComplete())
	{
		delete m_stage;
		m_stage = m_stageFactory->nextStage(++m_currentStage);
	}
}

void Boss::move()
{
	m_coords = m_stage->move( m_coords );
}

std::list< Shot * > Boss::shoot() noexcept
{
	return m_stage->shoot( m_coords );
}
