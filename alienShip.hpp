#ifndef _ALIEN_SHIP_
#define _ALIEN_SHIP_

#include "boss.hpp"
#include "Shot.hpp"

class AlienShip : public Boss
{
public:

	AlienShip() = delete;

	~AlienShip();

	AlienShip(POINT _coords);

	AlienShip(const AlienShip &) = delete;

	AlienShip & operator = (const AlienShip &) = delete;
};

#endif // !_ALIEN_SHIP_