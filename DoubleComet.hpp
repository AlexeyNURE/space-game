#pragma once

#include <iostream>

#include "Comet.hpp"
#include "CometKind.hpp"

class SimpleComet;

class DoubleComet
	:public Comet
{
public:
	DoubleComet(POINT _point, double _k, int _b, int _speed);
	~DoubleComet() = default;
	int getDamage() override;
	std::pair <SimpleComet *, SimpleComet *> destroy();
	CometKind getKind() const override;
};

inline int DoubleComet::getDamage()
{
	return m_Damage;
}

inline CometKind DoubleComet::getKind() const
{
	return CometKind::Double;
}