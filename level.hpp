#pragma once

#include <Windows.h>
#include <gdiplus.h>
#include <iostream>

class Boss;
class Comet;
class Enemy;
class StageFactory;
class DoubleComet;

//------------------------------------------------------------------------------

class Level
{
	const int m_pointsBossSpawn;

protected:

	Gdiplus::TextureBrush * m_brush;

	Level( int _pointsBossSpawn )
		:	m_pointsBossSpawn( _pointsBossSpawn )
	{
	}

	Level( const Level & ) = delete;

public:

	int getPointsWhenBossSpawns() const { return m_pointsBossSpawn; }

	Gdiplus::TextureBrush * getBrush() { return m_brush; }

	virtual RECT getPlayerRect() const;

	virtual Boss * createBoss() const = 0;

	virtual Comet * createComet( int _minimalSpeed ) const = 0;

	virtual Enemy * createEnemy() const { return nullptr; }

	virtual ~Level() = default;

	template< typename CometType >
	static CometType * spawnComet( int _minimalSpeed );
};

//------------------------------------------------------------------------------

class LevelOne : public Level
{
public:

	LevelOne();

	Boss * createBoss() const override;

	Comet * createComet( int _minimalSpeed ) const override;

	~LevelOne();
};

//------------------------------------------------------------------------------

class LevelTwo : public Level
{
public:

	LevelTwo();

	Boss * createBoss() const override;

	Comet * createComet( int _minimalSpeed ) const override;

	~LevelTwo();
};

//------------------------------------------------------------------------------

class Free : public Level
{
public:

	Free();

	Boss * createBoss() const override;

	Comet * createComet( int _minimalSpeed ) const override;

	RECT getPlayerRect() const override;

	Enemy * createEnemy() const override;

	~Free();
};

//------------------------------------------------------------------------------
