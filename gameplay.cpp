#include "draw.hpp"
#include "resource.h"

#include "Gameplay.hpp"
#include "level.hpp"

#include "wave.hpp"
#include "DoubleComet.hpp"
#include "SimpleComet.hpp"
#include "Shot.hpp"
#include "SpaceShip.hpp"
#include "enemy.hpp"

#include "boss.hpp"
#include "alienShip.hpp"
#include "stageFactory.hpp"
#include "alienShipStages.hpp"
#include "animations.hpp"

#include "SettingsFile.hpp"

#include <Windows.h>
#include <cmath>

#pragma comment (lib,"Gdiplus.lib")

extern HWND Wnd, WndMenu;
extern HINSTANCE hInst;

/*****************************************************************************/

Gameplay::Gameplay()
{
	m_ship = nullptr;
	m_boss = nullptr;
	m_wave = nullptr;
	m_bossThread = nullptr;
	m_moveThread = nullptr;
	m_bossDestroyAnimation = nullptr;

	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup( &gdiplusToken, &gdiplusStartupInput, NULL );
}

/*****************************************************************************/

Gameplay::~Gameplay()
{
	TerminateThread( m_bossThread, 0 );
	TerminateThread( m_moveThread, 0 );

	// clearing data after game
	clearData();

	// turning off the gdiplus
	GdiplusShutdown( gdiplusToken );
}

/*****************************************************************************/

void Gameplay::stopGame()
{
	m_gameOver = true;

	KillTimer( Wnd, TIMER );
	ShowWindow( Wnd, SW_HIDE );
	ShowWindow( WndMenu, SW_SHOWDEFAULT );

	SettingsFile file;
	file.open(std::ios_base::out | std::ios_base::in);
	file.writeLastResult(m_player);
	file.close();

	m_sounds.play( "sounds/menu_theme.wav", true );
	m_player.resetPoints();
	clearData();
}

/*****************************************************************************/

void Gameplay::initGame()
{
	SetTimer(Wnd, TIMER, 40, NULL);

	m_ship = new SpaceShip;
	m_gameOver = false;
	m_currentLevel = 0;
	m_wavesUsed = 0;
	m_pointsAfterPreviousLevel = 0;

	m_sounds.stop( "sounds/menu_theme.wav" );
	loadNextLevel();

	m_moveThread = CreateThread( NULL, NULL, ( LPTHREAD_START_ROUTINE )( &move ), this, NULL, NULL );
}

/*****************************************************************************/

void Gameplay::move(Gameplay & _gameplay)
{
	RECT rect;

	while ( !_gameplay.m_gameOver )
	{
		GetClientRect(Wnd, &rect);

		//-------------------------------------------

		_gameplay.moveComets();
		_gameplay.moveShots();
		_gameplay.moveWave();
		_gameplay.moveEnemies();

		_gameplay.checkEnemies();
		_gameplay.checkComets( rect );
		_gameplay.checkShots( rect );
		_gameplay.checkBossShots( rect );

		//-------------------------------------------

		Sleep( 50 );
	}
}

/*****************************************************************************/

void Gameplay::controllBoss( Gameplay & _gameplay )
{
	while ( !_gameplay.m_gameOver )
	{
		_gameplay.m_mutexForBoss.lock();

		_gameplay.m_boss->move();
		if ( _gameplay.objectIntersectsWithAnyPlayerShot( _gameplay.m_boss ) )
			_gameplay.m_boss->takeDamage( _gameplay.getShip()->m_ShotDamage );

		//----------------------------------------------------

		//if boss is dead - getting out of loop
		if ( !_gameplay.m_boss->isAlive() )
		{
			_gameplay.m_mutexForBoss.unlock();
			break;
		}
		_gameplay.m_boss->checkStage();

		//----------------------------------------------------

		auto shots = _gameplay.m_boss->shoot();

		_gameplay.m_mutexForBoss.unlock();
		_gameplay.m_mutexForBossShots.lock();

		//----------------------------------------------------

		// this check for situations, when boss appears he doesn't shoot
		// so method shoot returns empty list
		if ( !shots.empty() )
		{
			shots.sort();
			_gameplay.m_bossShots.merge( std::move( shots ) );
		}
		_gameplay.m_mutexForBossShots.unlock();

		//----------------------------------------------------

		Sleep( 50 );
	}
	_gameplay.bossFightFinished();
}

/*****************************************************************************/

void Gameplay::bossFightFinished()
{
	// if the game is not over we need to create boss destroy animation
	// and add points to player for killing boss
	if ( !m_gameOver )
	{
		m_player.increasePoints( 30 );

		m_bossDestroyAnimation = new Animation(
				new Image( L"images/boom.png" )
			,	m_boss->getCoords()
			,	7
			,	m_boss->getBitmapSideLenght()
		);
	}

	//----------------------------

	m_mutexForBoss.lock();

	delete m_boss;
	m_boss = nullptr;

	m_mutexForBoss.unlock();
}

/*****************************************************************************/

void Gameplay::moveComets()
{
	m_mutexForComet.lock();

	for ( auto it : m_comets )
		it->move();

	m_mutexForComet.unlock();
}

/*****************************************************************************/

void Gameplay::moveWave()
{
	m_mutexForWave.lock();

	if ( m_wave )
	{
		if ( m_wave->isValid() )
			m_wave->grow();
		else
		{
			delete m_wave;
			m_wave = nullptr;
		}
	}

	m_mutexForWave.unlock();
}

/*****************************************************************************/

void Gameplay::moveShots()
{
	m_mutexForShot.lock();

	for ( auto it : m_shots )
		it->move();

	m_mutexForShot.unlock();

	//------------------------

	m_mutexForBossShots.lock();

	for ( auto it : m_bossShots )
		it->move();

	m_mutexForBossShots.unlock();
}

/*****************************************************************************/

void Gameplay::moveEnemies()
{
	m_mutexForEnemies.lock();
	m_mutexForBossShots.lock();

	for ( auto it : m_enemies )
	{
		it->update();

		auto enemy_shot = it->createShot();
		if ( enemy_shot )
			m_bossShots.push_back( enemy_shot );
	}

	m_mutexForEnemies.unlock();
	m_mutexForBossShots.unlock();
}

/*****************************************************************************/

void Gameplay::checkComets(RECT _rect)
{
	m_mutexForComet.lock();

	Comet * tempComet = nullptr;
	for ( auto it : m_comets )
	{
		if ( tempComet )
		{
			m_comets.remove(tempComet);
			delete tempComet;
			tempComet = nullptr;
		}

		if ( !it->isValid( _rect ) )
			tempComet = it;
		else if ( objectIntersectsWithWave( it ) )
		{
			m_player.increasePoints();
			tempComet = it;
		}
	}
	if ( tempComet )
	{
		m_comets.remove(tempComet);
		delete tempComet;
		tempComet = nullptr;
	}

	m_mutexForComet.unlock();
}

/*****************************************************************************/

void Gameplay::checkShots(RECT _rect)
{
	m_mutexForShot.lock();
	Shot * tempShot = nullptr;
	for ( auto it : m_shots )
	{
		if ( tempShot )
		{
			m_shots.remove(tempShot);
			delete tempShot;
			tempShot = nullptr;
		}

		if ( !it->isValid(_rect) || shotIntersectsWithAnyComet(it) )
			tempShot = it;
	}
	if ( tempShot )
	{
		m_shots.remove(tempShot);
		delete tempShot;
		tempShot = nullptr;
	}
	m_mutexForShot.unlock();
}

/*****************************************************************************/

void Gameplay::checkBossShots( RECT _rect )
{
	m_mutexForBossShots.lock();
	Shot * tempShot = nullptr;
	for ( auto it : m_bossShots )
	{
		if ( tempShot )
		{
			m_bossShots.remove( tempShot );
			delete tempShot;
			tempShot = nullptr;
		}

		if ( !it->isValid( _rect ) || objectIntersectsWithWave( it ) )
			tempShot = it;
	}
	if ( tempShot )
	{
		m_bossShots.remove( tempShot );
		delete tempShot;
		tempShot = nullptr;
	}
	m_mutexForBossShots.unlock();
}

/*****************************************************************************/

void Gameplay::checkEnemies()
{
	m_mutexForEnemies.lock();
	Enemy * tempEnemy = nullptr;
	for ( auto it : m_enemies )
	{
		if ( tempEnemy )
		{
			m_enemies.remove( tempEnemy );
			delete tempEnemy;
			tempEnemy = nullptr;
		}
		if ( objectIntersectsWithAnyPlayerShot( it ) )
			it->takeDamage( m_ship->m_ShotDamage );

		if ( !it->isAlive() )
		{
			m_player.increasePoints( 3 );
			tempEnemy = it;
		}
	}
	if ( tempEnemy )
	{
		m_enemies.remove( tempEnemy );
		delete tempEnemy;
		tempEnemy = nullptr;
	}
	m_mutexForEnemies.unlock();
}

/*****************************************************************************/

void Gameplay::clearData()
{
	clearCometsAndShots();

	for ( auto it : m_enemies )
		delete it;
	m_enemies.clear();

	delete m_wave;
	m_wave = nullptr;

	delete m_ship;
	m_ship = nullptr;

	delete m_level;
	m_level = nullptr;

	delete m_boss;
	m_boss = nullptr;

	delete m_bossDestroyAnimation;
	m_bossDestroyAnimation = nullptr;
}

/*****************************************************************************/

void Gameplay::clearCometsAndShots()
{
	for ( auto it : m_shots )
		delete it;
	m_shots.clear();

	for ( auto it : m_bossShots )
		delete it;
	m_bossShots.clear();

	for ( auto it : m_comets )
		delete it;
	m_comets.clear();
}

/*****************************************************************************/

void Gameplay::draw( HDC _hdc )
{
	RECT rect;
	GetClientRect( Wnd, &rect );

	// dual bufferization
	HDC hdcMem = CreateCompatibleDC( _hdc );
	HBITMAP hbmMem = CreateCompatibleBitmap( _hdc, rect.right - rect.left, rect.bottom - rect.top );
	SelectObject( hdcMem, hbmMem );

	//------------------------------------------------------------------

	Graphics graphics( hdcMem );

	// drawing background
	graphics.FillRectangle( m_level->getBrush(), rect.left, rect.top, rect.right, rect.bottom );
	drawScore( graphics, hdcMem );
	drawWave( hdcMem, rect );
	drawShots( hdcMem );
	drawComets( hdcMem );
	drawBoss( graphics, hdcMem );
	drawEnemies( graphics );
	drawShip( graphics, rect );

	//-------------------------------------------------------------------

	BitBlt( _hdc, 0, 0, rect.right - rect.left, rect.bottom - rect.top, hdcMem, 0, 0, SRCCOPY );

	DeleteObject( hbmMem );
	DeleteDC( hdcMem );
}

/*****************************************************************************/

void Gameplay::drawBoss( Graphics & _graphics, HDC _hdc )
{
	static HBITMAP shot_bitmap = LoadBitmap( hInst, MAKEINTRESOURCE( ShotEnemy ) );

	m_mutexForBossShots.lock();

	for ( auto it : m_bossShots )
		DrawBitmap( _hdc, shot_bitmap, it->getXCoord(), it->getYCoord() );

	m_mutexForBossShots.unlock();

	//------------------------------------------------------------------------

	if ( m_bossDestroyAnimation )
	{
		m_bossDestroyAnimation->draw( _graphics );
		if ( m_bossDestroyAnimation->isEnded() )
		{
			delete m_bossDestroyAnimation;
			m_bossDestroyAnimation = nullptr;
		}
	}

	//------------------------------------------------------------------------

	if ( !m_boss )
		return;

	m_mutexForBoss.lock();

	_graphics.TranslateTransform(
			m_boss->getXCoord() + m_boss->getBitmapSideLenght() / 2
		,	m_boss->getYCoord() + m_boss->getBitmapSideLenght() / 2
	);

	_graphics.RotateTransform( m_boss->getAngle() );

	_graphics.DrawImage(
			m_boss->getBitmap()
		,	-(m_boss->getBitmapSideLenght() / 2)
		,	-(m_boss->getBitmapSideLenght() / 2)
		,	m_boss->getBitmapSideLenght()
		,	m_boss->getBitmapSideLenght()
	);

	_graphics.ResetTransform();

	drawBossHP( _hdc );

	m_mutexForBoss.unlock();
}

/*****************************************************************************/

void Gameplay::drawBossHP( HDC _hdc )
{
	RECT boundsForHPScale;
	GetClientRect( Wnd, &boundsForHPScale );

	boundsForHPScale.left += 50;
	boundsForHPScale.right -= 50;
	boundsForHPScale.bottom -= 50;
	boundsForHPScale.top = boundsForHPScale.bottom - 40;

	//----------------------------------------------------------

	static LOGBRUSH boundsBrushStyle;
	boundsBrushStyle.lbStyle = BS_NULL;
	static HBRUSH hBoundsBrush = CreateBrushIndirect( &boundsBrushStyle );

	const int boundsPenWidth = 5;
	static HPEN hBoundsPen = CreatePen( PS_SOLID, boundsPenWidth, RGB( 255, 255, 255 ) );

	SelectObject( _hdc, hBoundsBrush );
	SelectObject( _hdc, hBoundsPen );

	Rectangle( 
			_hdc
		,	boundsForHPScale.left
		,	boundsForHPScale.top
		,	boundsForHPScale.right
		,	boundsForHPScale.bottom
	);

	//----------------------------------------------------------

	static HBRUSH hHPBrush = CreateSolidBrush( RGB( 200, 0, 0 ) );
	static HPEN hHPPen = CreatePen( PS_NULL, 0, 0 );

	SelectObject( _hdc, hHPBrush );
	SelectObject( _hdc, hHPPen );

	RECT hpScale = boundsForHPScale;
	hpScale.left += boundsPenWidth;
	hpScale.top += boundsPenWidth;
	hpScale.bottom -= boundsPenWidth;
	hpScale.right -= boundsPenWidth;

	double oneProcentOfHP = m_boss->getMaximalHP() / 100.0; // 1% of max HP
	double lenghtOfEachProcent = ( hpScale.right - hpScale.left ) / 100.0; // in pixels
	hpScale.right = ( ( double )m_boss->getCurrentHP() / oneProcentOfHP ) * lenghtOfEachProcent + ( double )hpScale.left;

	Rectangle(
			_hdc
		,	hpScale.left
		,	hpScale.top
		,	hpScale.right
		,	hpScale.bottom
	);
}

/*****************************************************************************/

void Gameplay::drawEnemies( Graphics & _graphics )
{
	m_mutexForEnemies.lock();

	for ( auto it : m_enemies )
	{
		_graphics.TranslateTransform(
				it->getXCoord() + it->getBitmapSideLenght() / 2
			,	it->getYCoord() + it->getBitmapSideLenght() / 2
		);

		_graphics.RotateTransform( it->getAngle() );

		_graphics.DrawImage(
				it->getImage()
			,	-( it->getBitmapSideLenght() / 2 )
			,	-( it->getBitmapSideLenght() / 2 )
			,	it->getBitmapSideLenght()
			,	it->getBitmapSideLenght()
		);

		_graphics.ResetTransform();
	}

	m_mutexForEnemies.unlock();
}

/*****************************************************************************/

void Gameplay::drawShip( Graphics & _graphics, RECT _rect )
{
	Image heart( L"images/heart.png" );

	const int eachHeartWidth{ 30 };
	POINT drawCoord{ _rect.right, 0 };

	int heartsToDraw{ m_ship->getCurrentHP() / 10 };
	for ( int i{ 1 }; i <= heartsToDraw; ++i )
		_graphics.DrawImage(
				&heart
			,	drawCoord.x - i * eachHeartWidth
			,	drawCoord.y
		);

	//-------------------------------------------------------------------------

	_graphics.TranslateTransform( 
			m_ship->getXCoord() + m_ship->getBitmapSideLenght() / 2
		,	m_ship->getYCoord() + m_ship->getBitmapSideLenght() / 2
	);

	_graphics.RotateTransform( m_ship->getAngle() + 180 );

	_graphics.DrawImage(
			m_ship->getImage()
		,	-( m_ship->getBitmapSideLenght() / 2 )
		,	-( m_ship->getBitmapSideLenght() / 2 )
		,	m_ship->getBitmapSideLenght()
		,	m_ship->getBitmapSideLenght()
	);

	_graphics.ResetTransform();
}

/*****************************************************************************/

void Gameplay::drawShots( HDC _hdc )
{
	static HBITMAP shot_bitmap = LoadBitmap( hInst, MAKEINTRESOURCE( ShotPlayer ) );

	m_mutexForShot.lock();

	for ( auto it : m_shots )
		DrawBitmap(_hdc, shot_bitmap, it->getXCoord(), it->getYCoord());

	m_mutexForShot.unlock();
}

/*****************************************************************************/

void Gameplay::drawComets( HDC _hdc )
{
	static HBITMAP comet_bitmap = LoadBitmap(hInst, MAKEINTRESOURCE(Asteroid));
	static HBITMAP double_comet = LoadBitmap(hInst, MAKEINTRESOURCE(DoubleAsteroid));

	m_mutexForComet.lock();

	for (auto it : m_comets)
	{	
		if (it->getKind() == CometKind::Double)
			DrawBitmap(_hdc, double_comet, it->getXCoord(), it->getYCoord());
		else
			DrawBitmap(_hdc, comet_bitmap, it->getXCoord(), it->getYCoord());
	}

	m_mutexForComet.unlock();
}

/*****************************************************************************/

void Gameplay::drawWave( HDC _hdc, RECT _rect ) const
{
	// brush for trasparent background
	static LOGBRUSH lBrush{ BS_NULL, NULL, NULL };
	static HBRUSH brush = CreateBrushIndirect( &lBrush );

	static HPEN penForIndicatingWaveReady = CreatePen( PS_SOLID, 4, RGB( 0, 0, 255 ) );

	if ( isWaveReady() )
	{
		SelectObject( _hdc, brush );
		SelectObject( _hdc, penForIndicatingWaveReady );

		int wavesAvailable{ getPossibleWavesAmount() - m_wavesUsed };
		for ( int i{ 0 }; i < wavesAvailable; ++i )
			Ellipse(
				_hdc
				, _rect.right - 28 - 30 * i
				, _rect.top + 40
				, _rect.right - 2 - 30 * i
				, _rect.top + 66
			);
	}

	if ( !m_wave )
		return;

	HPEN pen = CreatePen(PS_SOLID, 10, RGB(0, 0, m_wave->getColorDepth()));

	SelectObject(_hdc, brush);
	SelectObject(_hdc, pen);

	POINT waveCentreCoords = m_wave->getCentreCoords();
	Ellipse(_hdc, waveCentreCoords.x - m_wave->getRadius(), waveCentreCoords.y - m_wave->getRadius(),
		waveCentreCoords.x + m_wave->getRadius(), waveCentreCoords.y + m_wave->getRadius());

	DeleteObject( pen );
}

/*****************************************************************************/

void Gameplay::drawScore( Graphics & _graphics, HDC _hdc ) const
{
	Font font( &FontFamily( L"Times new roman" ), 14 );
	SolidBrush brush( Color( 255, 255, 255 ) );

	std::wstring score = L"Your score: ";
	score += std::to_wstring( m_player.getPoints() );

	_graphics.DrawString( score.c_str(), score.size(), &font, PointF( 0, 0 ), &brush );
}

/*****************************************************************************/

void Gameplay::spawnComet()
{
	if ( m_comets.size() >= m_maxAmountComets )
		return;

	m_mutexForComet.lock();

	Comet * res = m_level->createComet( m_cometMinimalSpeed );
	if ( res )
		m_comets.push_back( res );

	m_mutexForComet.unlock();
}

/*****************************************************************************/

void Gameplay::spawnShot()
{
	if ( m_shots.size() >= m_maxAmountShots )
		return;

	m_sounds.play("sounds/shot1.wav", true);

	m_mutexForShot.lock();
	m_shots.push_back( m_ship->createShot() );
	m_mutexForShot.unlock();
}

/*****************************************************************************/

void Gameplay::spawnWave()
{
	if ( m_wave || !isWaveReady() )
		return;

	m_mutexForWave.lock();
	m_wave = m_ship->createWave();
	m_sounds.play("sounds/wave.wav", true);
	m_mutexForWave.unlock();

	++m_wavesUsed;
}

/*****************************************************************************/

void Gameplay::spawnBoss()
{
	if ( m_player.getPoints() > m_level->getPointsWhenBossSpawns() + m_pointsAfterPreviousLevel
		 && !m_bossSpawnedOnCurrentLevel )
	{
		m_boss = m_level->createBoss();
		m_bossSpawnedOnCurrentLevel = true;

		m_bossThread = CreateThread( NULL, NULL, ( LPTHREAD_START_ROUTINE ) ( &controllBoss ), this, NULL, NULL );
	}
}

/*****************************************************************************/

void Gameplay::spawnEmemy()
{
	static int lastTimeSpawned{}; // points

	if ( m_player.getPoints() > lastTimeSpawned + m_pointsForSpawnEnemy )
	{
		auto enemy = m_level->createEnemy();
		if ( enemy )
		{
			lastTimeSpawned = m_player.getPoints();
			m_enemies.push_back( enemy );
		}
	}
}

/*****************************************************************************/

void Gameplay::controlDifficulty()
{
	m_cometMinimalSpeed = m_player.getPoints() / 50 + 3;
	m_maxAmountComets = m_player.getPoints() / 20 + 10;
}

/*****************************************************************************/

void Gameplay::loadNextLevel()
{
	delete m_level;
	delete m_wave;

	m_level = m_levelFactory.nextLevel( m_currentLevel );
	++m_currentLevel;

	m_bossSpawnedOnCurrentLevel = false;
	m_shipAppears = true;
	m_wave = nullptr;

	RECT rt;
	GetClientRect( Wnd, &rt );

	POINT coords;
	coords.x = ( rt.right - rt.left ) / 2 - m_ship->getBitmapSideLenght() / 2;
	coords.y = rt.bottom;

	m_ship->setCoordinates( coords );
	m_pointsAfterPreviousLevel = m_player.getPoints();

	clearCometsAndShots();
}

/*****************************************************************************/

// choosing how to move ship, and checking level
RECT Gameplay::chooseShipWay()
{
	RECT rt;
	GetClientRect( Wnd, &rt );
	if ( currentLevelComplete() )
	{
		m_ship->resetAngle();

		if ( m_ship->getYCoord() > -( m_ship->getBitmapSideLenght() ) )
		{
			rt.top -= m_ship->getBitmapSideLenght() + 10;

			m_ship->move_down = false;
			m_ship->move_up = true;
		}
		else
			loadNextLevel();
	}
	// if ship is only appearing into the battlefield, it shold move until it gets to the right start point
	else if ( m_shipAppears )
	{
		m_ship->resetMoving();

		// here we check does the ship really get into the battlefield
		if ( m_ship->getYCoord() <= rt.bottom - m_ship->getBitmapSideLenght() )
			m_shipAppears = false;
		// otherwise, we still moving ship upper
		else
		{
			m_ship->move_up = true;
			rt.bottom += m_ship->getBitmapSideLenght();
		}
	}
	else
		rt = m_level->getPlayerRect();

	return rt;
}

/*****************************************************************************/

bool Gameplay::shotIntersectsWithAnyComet(Shot * _pShot)
{
	m_mutexForComet.lock();
	for (auto it : m_comets)
	{
		if (FlyingObject::intersects(*_pShot, *it))
		{
			if( it->getKind() == CometKind::Double )
			{
				DoubleComet * double_com = dynamic_cast<DoubleComet *> ( it );
				auto res = double_com->destroy();
				m_comets.push_back( res.first );
				m_comets.push_back( res.second );
			}
			m_comets.remove( it );
			delete it;

			m_player.increasePoints();
			m_sounds.play("sounds/crash1.wav", true);

			m_mutexForComet.unlock();
			return true;
		}
	}
	m_mutexForComet.unlock();
	return false;
}

/*****************************************************************************/

bool Gameplay::objectIntersectsWithWave(FlyingObject * _obj)
{
	if ( !m_wave )
		return false;

	POINT cometCentreCoords = _obj->getCentreCoords();
	POINT waveCentreCoords = m_wave->getCentreCoords();

	int radius = sqrt( pow( ( cometCentreCoords.x - waveCentreCoords.x ), 2 ) +
		pow( ( cometCentreCoords.y - waveCentreCoords.y ), 2 ) );

	if ( radius < m_wave->getRadius() + 20 )
		return true;

	return false;
}

/*****************************************************************************/

// in this function we checking does the ship intersects with something in current moment
void Gameplay::checkShip()
{
	// ship can intersect with comets or shots
	// in both cases ship taking damage and the comet or shot is deleting
	// in first part we checking intersections with comets
	m_mutexForComet.lock();

	Comet * tempComet = nullptr;
	for ( auto it : m_comets )
	{
		if ( tempComet )
		{
			m_comets.remove( tempComet );
			delete tempComet;
			tempComet = nullptr;
		}

		if ( FlyingObject::intersects( *m_ship, *it ) )
		{
			tempComet = it;
			m_ship->takeDamage( it->getDamage() );
			m_sounds.play( "sounds/crash2.wav", true );
		}
	}
	if ( tempComet )
	{
		m_comets.remove( tempComet );
		delete tempComet;
	}

	m_mutexForComet.unlock();

	// in second part we checking intersections with boss shots
	shipIntersectsWithBossShot();
}

/*****************************************************************************/

bool Gameplay::objectIntersectsWithAnyPlayerShot( FlyingObject * _obj )
{
	m_mutexForShot.lock();

	for ( auto it : m_shots )
	{
		if ( FlyingObject::intersects(*_obj, *it) )
		{
			m_shots.remove(it);
			delete it;

			m_mutexForShot.unlock();
			return true;
		}
	}
	m_mutexForShot.unlock();
	return false;
}

/*****************************************************************************/

bool Gameplay::shipIntersectsWithBossShot()
{
	m_mutexForBossShots.lock();

	for ( auto it : m_bossShots )
	{
		if ( FlyingObject::intersects( *m_ship, *it ) )
		{
			m_ship->takeDamage(it->getDamage());
			m_bossShots.remove( it );
			delete it;

			m_sounds.play( "sounds/crash2.wav", true );

			m_mutexForBossShots.unlock();
			return true;
		}
	}

	m_mutexForBossShots.unlock();
	return false;
}

/*****************************************************************************/

void Gameplay::update()
{
	checkShip();

	if ( !m_ship->isAlive() )
	{
		stopGame();
		return;
	}

	if ( !m_boss && !m_bossSpawnedOnCurrentLevel && !m_gameOver )
	{
		controlDifficulty();

		spawnBoss();
		spawnEmemy();
		spawnComet();
	}

	m_ship->update( chooseShipWay() );

	InvalidateRect(Wnd, NULL, TRUE);
}

/*****************************************************************************/
