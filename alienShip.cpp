#include "alienShip.hpp"
#include "resource.h"
#include "alienShipStages.hpp"

#include <windows.h>

extern HINSTANCE hInst;

AlienShip::~AlienShip()
{
	delete m_stage;
	delete m_bossBitmap;
	delete m_stageFactory;
}

AlienShip::AlienShip( POINT _coords )
	:	Boss( 300, 3, _coords, 200 )
{
	m_bossBitmap = new Gdiplus::Image( L"images/boss1.png" );
	m_stageFactory = new AlienShipStageFactory;
	m_stage = m_stageFactory->nextStage( m_currentStage );
}