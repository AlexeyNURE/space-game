#include "Comet.hpp"

Comet::Comet(POINT _point, double _k, int _b, int _speed, int _bitmap_lenght)
	:FlyingObject(_bitmap_lenght)
{
	m_speed = _speed;
	m_coords = _point;
	m_k = _k;
	m_b = _b;
	m_yBegin = m_k * (double)m_coords.x + m_b - m_coords.y;
}

void Comet::move()
{
	if (m_k > 0)
		m_coords.x += m_speed;
	else
		m_coords.x -= m_speed;

	m_coords.y = m_k * (double)m_coords.x + m_b - m_yBegin;
}

void Comet::incrementSpeed()
{
	m_speed++;
}