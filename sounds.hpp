#ifndef _SOUNDS_HPP_
#define _SOUNDS_HPP_

#include "audiere.h"
#include <iostream>
#include <map>

using namespace audiere;

class Sound
{
private:
	AudioDevicePtr m_device;
	std::map<std::string, OutputStreamPtr> m_Sounds;
public:
	Sound();
	~Sound();
	void play(std::string const &_name, bool is_thread);
	void stop(std::string const &_name);
	void clear();
	OutputStreamPtr & getSound(std::string const &_name);

};


inline OutputStreamPtr & Sound::getSound(std::string const &_name)
{
	return m_Sounds[_name];
}


#endif // !_SOUNDS_HPP_
