#ifndef _FILEMANAGER_HPP_
#define _FILEMANAGER_HPP_

#include <iostream>
#include <fstream>
#include <cassert>
#include <deque>
#include <list>
#include <set>
#include <experimental\filesystem>
#include <functional>

class FileManager
{
private:
	std::fstream m_stream;
	std::string m_fileName;

public:
	FileManager();
	virtual ~FileManager();
	void open(unsigned int _mode, std::string const &_s = "cfg/settings.txt");
	void close();
	FileManager(const FileManager &) = delete;
	FileManager operator = (const FileManager &) = delete;
	void clearFile();
	bool is_empty();
	std::fstream & getStream()
	{
		return m_stream;
	}
};

static std::string ExtractString(std::string source, std::string start, std::string end)
{
	std::size_t startIndex = source.find(start);
	if (startIndex == std::string::npos)
		return "";

	startIndex += start.length();
	std::string::size_type endIndex = source.find(end, startIndex);
	return source.substr(startIndex, endIndex - startIndex);
}


#endif // !_FILEmANAGER_HPP_
