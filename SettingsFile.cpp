#include "SettingsFile.hpp"
#include "Player.hpp"
SettingsFile::SettingsFile()
	:FileManager()
{
	if (!std::experimental::filesystem::exists("cfg/settings.txt"))
	{
		getStream().open("cfg/settings.txt", std::ios_base::out);
	}
	getStream().close();
}

SettingsFile::~SettingsFile()
{
	if (getStream().is_open())
		getStream().close();
}

void SettingsFile::init()
{
	std::string currentLine;
	std::getline(getStream(), currentLine);
	std::string name;
	int score;

	std::getline(getStream(), currentLine);

	while (currentLine != "Best results:")
	{
		name = ExtractString(currentLine, ".", " ");
		score = atoi(ExtractString(currentLine, "\"", "\"").c_str());
		m_LastResults.push_back(std::make_pair(name, score));
		std::getline(getStream(), currentLine);
	}
	while (!getStream().eof())
	{
		std::getline(getStream(), currentLine);
		name = ExtractString(currentLine, ".", " ");
		score = atoi(ExtractString(currentLine, "\"", "\"").c_str());
		m_BestResults.insert(std::make_pair(name, score));
	}
	clearFile();
}

void SettingsFile::writeLastResult(const Player &_player)
{
	if (!is_empty())
		init();  // if file is empty - no need to init it
	getStream().seekg(0, std::ios_base::beg);  // go to the beginning of the file
	m_LastResults.push_front(std::make_pair(_player.getName(), _player.getPoints()));
	if (m_LastResults.size() > MAX_AMOUNT_RESULTS)
		m_LastResults.pop_back();
	getStream() << "Last results:";
	for (int i(0); i < m_LastResults.size(); i++)
		getStream() << '\n' << i + 1 << '.' << m_LastResults[i].first << " scored: " << '\"' << m_LastResults[i].second << '\"';  // writing last results
	getStream() << "\nBest results:";
	if (m_BestResults.size() < MAX_AMOUNT_RESULTS)
		writeNewRecord();  // new best result
	else if (m_LastResults.front().second > m_BestResults.begin()->second)
		writeNewRecord();

	int i = 1;
	for (auto it = m_BestResults.rbegin(); it != m_BestResults.rend(); it++)
		getStream() << '\n' << i++ << '.' << it->first << " scored: " << '\"' << it->second << '\"'; // writing top results
}

void SettingsFile::writeNewRecord()
{
	m_BestResults.insert(m_LastResults.front());
	if (m_BestResults.size() > MAX_AMOUNT_RESULTS)
		m_BestResults.erase(m_BestResults.begin());
}

std::pair<std::string, int> SettingsFile::getLastResult() const
{
	return m_LastResults.front();
}

std::pair<std::string, int> SettingsFile::getTopResult() const
{
	return *m_BestResults.rbegin();
}

void SettingsFile::forEachLastResults(std::function<void(score const &)> _action) const
{
	for (auto const &it : m_LastResults)
		_action(it);
}

void SettingsFile::forEachTopResults(std::function<void(score const &)> _action) const
{
	for (auto const &it : m_BestResults)
		_action(it);
}