#include <windows.h>
#include <iostream>
#include <mutex>
#include <ctime>
#include "audiere.h"
#include "Buttons.hpp"
#include "SettingsFile.hpp"
#include "ConfigFile.hpp"
#include "resource.h"

#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

#include "Gameplay.hpp"
#include "moveSide.hpp"
#include "menu.hpp"
#define TIMER 1002

HWND Wnd, WndMenu;
HWND StartButton, QuitButton, StatsButton, BackButton, PlayerButton, EditButton, OKButton, ResetButton;
HINSTANCE hInst;
Gameplay gameplay;
Menu menu;
LPCTSTR szWindowClass = "Main window";
LPCTSTR szWindowClassMenu = "Menu";
LPCTSTR szTitle = "Space";

ATOM MyRegisterClass(HINSTANCE hInstance);
ATOM MyRegisterClassMenu(HINSTANCE hInstance);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndProcMenu(HWND, UINT, WPARAM, LPARAM);
HANDLE hInstAccel;

int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	MSG msg;
	MyRegisterClass(hInstance);
	MyRegisterClassMenu(hInstance);

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	hInst = hInstance;

	Wnd = CreateWindow(szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	WndMenu = CreateWindowA(szWindowClassMenu,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (!Wnd || !WndMenu)
		return FALSE;

	StartButton = CreateWindow("BUTTON", "Start Game", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 80, 50, 120, 50, WndMenu, (HMENU)Buttons::Start, hInst, NULL);
	StatsButton = CreateWindow("BUTTON", "Game Statistics", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 80, 120, 120, 50, WndMenu, (HMENU)Buttons::Stats, hInst, NULL);
	PlayerButton = CreateWindow("BUTTON", "Change player", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 80, 190, 120, 50, WndMenu, (HMENU)Buttons::PlayerMenu, hInst, NULL);
	QuitButton = CreateWindow("BUTTON", "Quit Game", WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON, 80, 260, 120, 50, WndMenu, (HMENU)Buttons::Quit, hInst, NULL);
	BackButton = CreateWindow("BUTTON", "<== Go back", SW_HIDE | WS_CHILD | BS_PUSHBUTTON, 80, 50, 120, 50, WndMenu, (HMENU)Buttons::Back, hInst, NULL);
	OKButton = CreateWindow("BUTTON", "OK", SW_HIDE | WS_CHILD | BS_PUSHBUTTON, 80, 250, 120, 50, WndMenu, (HMENU)Buttons::OK, hInst, NULL);
	EditButton = CreateWindow("Edit", 0, SW_HIDE | WS_BORDER | WS_CHILD, 80, 200, 120, 20, WndMenu, (HMENU)Buttons::Edit, hInst, NULL);
	ResetButton = CreateWindow("BUTTON", "Reset statistics", SW_HIDE | WS_CHILD | BS_PUSHBUTTON, 80, 450, 120, 50, WndMenu, (HMENU)Buttons::Reset, hInst, NULL);

	ShowWindow(WndMenu, nCmdShow);
	UpdateWindow(WndMenu);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	GdiplusShutdown(gdiplusToken);

	return msg.wParam;
}

ATOM MyRegisterClassMenu(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW | WS_MAXIMIZEBOX;
	wcex.lpfnWndProc = (WNDPROC)WndProcMenu;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(NULL, IDI_HAND);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = NULL;
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClassMenu;
	wcex.hIconSm = NULL;

	return RegisterClassEx(&wcex);
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(NULL, IDI_HAND);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = NULL;
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = NULL;

	return RegisterClassEx(&wcex);
}

LRESULT CALLBACK WndProcMenu(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		gameplay.getSound().play("sounds/menu_theme.wav", true);
		break;
	case WM_COMMAND:
		switch (wParam)
		{
		case Buttons::Start:
			menu.press(Buttons::Start);
			break;
		case Buttons::Stats:
			menu.press(Buttons::Stats);
			break;
		case Buttons::Quit:
			menu.press(Buttons::Quit);
			break;
		case Buttons::Back:
			menu.press(Buttons::Back);
			break;
		case Buttons::PlayerMenu:
			menu.press(Buttons::PlayerMenu);
			break;
		case Buttons::OK:
			menu.press(Buttons::OK);
			break;
		case Buttons::Reset:
			menu.press(Buttons::Reset);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_KEYDOWN:
		break;

	case WM_ERASEBKGND:
	{
		hdc = (HDC)wParam;
		menu.drawBackground(hdc);
	}

	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		menu.draw(hdc);

		EndPaint(hWnd, &ps);
		break;
	}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	switch (message)
	{
	case WM_TIMER:
		gameplay.update();
		break;

	case WM_KEYUP:
		switch (LOWORD(wParam))
		{
		case 'A':
			gameplay.getShip()->move_left = false;
			break;
		case 'D':
			gameplay.getShip()->move_right = false;
			break;
		case 'W':
			gameplay.getShip()->move_up = false;
			break;
		case 'S':
			gameplay.getShip()->move_down = false;
			break;
		case VK_LEFT:
			gameplay.getShip()->rotate_left = false;
			break;
		case VK_RIGHT:
			gameplay.getShip()->rotate_right = false;
			break;
		}
		break;

	case WM_ERASEBKGND:
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case 'A':
			gameplay.getShip()->move_left = true;
			break;
		case 'D':
			gameplay.getShip()->move_right = true;
			break;
		case 'W':
			gameplay.getShip()->move_up = true;
			break;
		case 'S':
			gameplay.getShip()->move_down = true;
			break;
		case 'E':
			gameplay.spawnWave();
			break;
		case VK_SPACE:
			gameplay.spawnShot();
			break;
		case VK_LEFT:
			gameplay.getShip()->rotate_left = true;
			break;
		case VK_RIGHT:
			gameplay.getShip()->rotate_right = true;
			break;
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		gameplay.draw(hdc);

		EndPaint(hWnd, &ps);
		break;

	case WM_CREATE:
		srand(time(NULL));
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}