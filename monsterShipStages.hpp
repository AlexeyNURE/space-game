#ifndef _MONSTERSHIPSTAGES_HPP
#define _MONSTERSHIPSTAGES_HPP

#include "stage.hpp"
#include "stageFactory.hpp"

#include <list>

class MonsterShip;
class Shot;

/******************************************************/

class MonsterShipStageDissappear : public Stage
{
public:

	MonsterShipStageDissappear();

	MonsterShipStageDissappear(const MonsterShipStageDissappear &) = delete;

	MonsterShipStageDissappear & operator = (const MonsterShipStageDissappear &) = delete;

	POINT move(POINT _coords) override;

	std::list< Shot * > shoot(POINT _coords) override;

	bool isComplete() const noexcept override;

};

class MonsterShipStageAppear : public Stage
{
public:

	MonsterShipStageAppear();

	MonsterShipStageAppear(const MonsterShipStageAppear &) = delete;

	MonsterShipStageAppear & operator = (const MonsterShipStageAppear &) = delete;

	POINT move(POINT _coords) override;

	std::list< Shot * > shoot( POINT _coords ) override;

	bool isComplete() const noexcept override;
};


class MonsterShipStageFactory : public StageFactory
{
public:

	MonsterShipStageFactory() = default;

	MonsterShipStageFactory(const MonsterShipStageFactory &) = delete;

	MonsterShipStageFactory & operator = (const MonsterShipStageFactory &) = delete;

	Stage * nextStage(int _currentStage) const override;
};


#endif