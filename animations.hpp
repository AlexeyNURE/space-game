#pragma once

#include <windows.h>
#include <gdiplus.h>
#include <iostream>

using namespace Gdiplus;

class Animation
{
	const std::size_t m_framesAmount;

	POINT m_coords;

	Image * m_image;

	int m_currentFrame;

	int m_imageSideLenght;

public:

	Animation( Image * _image, POINT _coords, std::size_t _framesAmount, int _imageSideLenght )
		:	m_framesAmount( _framesAmount )
		,	m_currentFrame( 0 )
		,	m_coords( _coords )
		,	m_imageSideLenght( _imageSideLenght )
		,	m_image( _image )
	{
	}

	bool isEnded() const noexcept
	{
		return m_currentFrame == m_framesAmount;
	}

	void draw( Graphics & _graphics ) noexcept;
};
