#ifndef _BOSS_
#define _BOSS_

#include "rotatbleObject.hpp"
#include "stageFactory.hpp"
#include "stage.hpp"

#include <gdiplus.h>
#include <list>

class Boss : public RotatbleObject
{
protected:

	const int m_maxHP;

	int m_currentHP;

	//--------------------------------

	const int m_stagesCount;

	int m_currentStage;

	Stage * m_stage;

	StageFactory * m_stageFactory;

	Gdiplus::Image * m_bossBitmap;

	//--------------------------------

	Boss(int _maxHP, int _stagesCount, POINT _coords, int _bitmapSideLenght);

public:

	int getCurrentHP() const noexcept { return m_currentHP; }

	int getMaximalHP() const noexcept { return m_maxHP; }

	int getStagesCount() const noexcept { return m_stagesCount; }

	int getCurrentStage() const noexcept { return m_currentStage; }

	Gdiplus::Image * getBitmap() const noexcept { return m_bossBitmap; }

	//----------------------------------------------------------------------

	bool isAlive() const noexcept { return m_currentHP > 0; }

	void takeDamage(int _damage) noexcept { m_currentHP -= _damage; }

	//----------------------------------------------------------------------

	virtual void checkStage() noexcept;

	virtual void move();

	virtual std::list< Shot * > shoot() noexcept;

	//----------------------------------------------------------------------

	virtual ~Boss() = default;
};

#endif // !_BOSS_