#ifndef _COMET_
#define _COMET_

#include "FlyingObject.hpp"
#include "CometKind.hpp"

#include <Windows.h>

class Comet : public FlyingObject
{
public:

	Comet() = delete;

	Comet(const Comet &) = delete;

	virtual ~Comet() = default;

	void incrementSpeed();

	virtual void move();

	virtual int getDamage() = 0;

	virtual CometKind getKind() const = 0;

protected:

	Comet(POINT _point, double _k, int _b, int _moveSpeed, int _bitmap_lenght);

	Comet & operator = (const Comet &) = delete;

	int m_speed;

	double m_k;

	int m_b;

	int m_yBegin;

	int m_Damage;
};

#endif // !_COMET_