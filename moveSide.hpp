#ifndef _MOVE_SIDE_
#define _MOVE_SIDE_

enum class MoveSide { LEFT = -1, RIGHT = 1, UP, DOWN };
enum class RotateSide { LEFT = -1, RIGHT = 1 };
enum class SpawnSide { LEFT, RIGHT };

#endif // !_MOVE_SIDE_