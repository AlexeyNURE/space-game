#ifndef _SHOT_
#define _SHOT_

#include "FlyingObject.hpp"

#include <cmath>
#include <Windows.h>

class Shot : public FlyingObject
{
	static const int m_moveSpeed{ 25 };

	HBITMAP * m_bitmap;

	int m_angle;

	int m_Damage;

public:

	Shot() = default;

	Shot(const Shot &) = delete;

	Shot(POINT _coords, int _angle, int _damage);

	Shot & operator = (const Shot &) = delete;

	void move() noexcept;

	int getDamage() const noexcept;
}; 

inline int Shot::getDamage() const noexcept
{
	return m_Damage;
}

#endif // !_SHOT_