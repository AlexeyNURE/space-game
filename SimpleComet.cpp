#include "SimpleComet.hpp"

SimpleComet::SimpleComet(POINT _point, double _k, int _b, int _speed)
	:Comet(_point, _k, _b, _speed, 37)
{
	m_Damage = 10;
}

int SimpleComet::getDamage()
{
	return m_Damage;
}