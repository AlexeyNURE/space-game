#ifndef _WAVE_HPP_
#define _WAVE_HPP_

#include <Windows.h>

class Wave
{
	const static int radiusIncreasing{ 12 };

	const static int startRadius{ 40 };

	const static int maximalRadius{ 400 };

	POINT m_centreCoords;

	int m_radius;

	int m_colorBlue;

public:

	Wave() = delete;

	Wave( POINT _coords );

	Wave(Wave &) = delete;

	Wave & operator = (Wave &) = delete;

	void grow() noexcept;

	POINT getCentreCoords() const noexcept { return m_centreCoords; }

	int getRadius() const noexcept { return m_radius; }

	int getColorDepth() const noexcept { return m_colorBlue; }

	bool isValid() noexcept { return m_radius < maximalRadius; }
};

#endif