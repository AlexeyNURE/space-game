#pragma once
#include "Buttons.hpp"
#include <Windows.h>
#include "SettingsFile.hpp"
#include "ConfigFile.hpp"

class Menu
{
private:
	bool isStats;
	bool isMenu;
	bool isPlayerMenu;
	SettingsFile settings;
	ConfigFile config;
	static const int ButtonX{ 80 };
public:
	Menu();
	~Menu();
	void draw(HDC hdc);
	void press(Buttons _buttons);
	void showStats();
	void showMenu();
	void showPlayer();
	void drawBackground(HDC _hdc);

};