#ifndef _ALIEN_SHIP_STAGES_
#define _ALIEN_SHIP_STAGES_

#include "stage.hpp"
#include "stageFactory.hpp"

#include <list>

class AlienShip;
class Shot;

/******************************************************/

class AppearStage : public Stage
{
public:

	AppearStage();

	POINT move( POINT _coords ) override;

	std::list< Shot * > shoot( POINT _coords ) override;

	bool isComplete() const noexcept override;
};

/******************************************************/

class AlienShipStageOne : public Stage
{
public:

	AlienShipStageOne();

	AlienShipStageOne(const AlienShipStageOne &) = delete;

	AlienShipStageOne & operator = (const AlienShipStageOne &) = delete;

	POINT move(POINT _coords) override;

	std::list< Shot * > shoot(POINT _coords) override;

	bool isComplete() const noexcept override;
};

/******************************************************/

class AlienShipStageTwo : public Stage
{
	const int m_shootAngle{ 120 };

	const int m_shootStep{ 20 };

public:

	AlienShipStageTwo();

	AlienShipStageTwo(const AlienShipStageTwo &) = delete;

	AlienShipStageTwo & operator = (const AlienShipStageTwo &) = delete;

	POINT move(POINT _coords) override;

	std::list< Shot * > shoot(POINT _coords) override;

	bool isComplete() const noexcept override;
};

/******************************************************/

class AlienShipStageThree : public Stage
{
	const int m_delta{ 200 };

public:

	AlienShipStageThree();

	AlienShipStageThree( const AlienShipStageThree & ) = delete;

	AlienShipStageThree & operator = (const AlienShipStageThree &) = delete;

	POINT move(POINT _coords) override;

	std::list< Shot * > shoot(POINT _coords) override;

	bool isComplete() const noexcept override;
};

/******************************************************/

class AlienShipStageFactory : public StageFactory
{
public:

	AlienShipStageFactory() = default;

	AlienShipStageFactory(const AlienShipStageFactory &) = delete;

	AlienShipStageFactory & operator = (const AlienShipStageFactory &) = delete;

	Stage * nextStage(int _currentStage) const override;
};

/******************************************************/

#endif // !_ALIEN_SHIP_STAGES_