#include "FileManager.hpp"
#include "Player.hpp"

FileManager::FileManager()
{
}

void FileManager::open(unsigned int _mode, std::string const &_s)
{
	assert(!_s.empty());
	m_stream.open(_s, _mode);
	assert(m_stream);
}

bool FileManager::is_empty()
{
	return m_stream.peek() == std::fstream::traits_type::eof();	
}

FileManager::~FileManager()
{
	if (m_stream.is_open())
	m_stream.close();
}

void FileManager::close()
{
	m_stream.close();
}

void FileManager::clearFile()
{
	std::ofstream ofs;
	ofs.open("cfg/settings.txt", std::ofstream::out | std::ofstream::trunc);
	ofs.close();
}