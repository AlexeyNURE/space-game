#include "ConfigFile.hpp"
ConfigFile::ConfigFile()
	:FileManager()
{
	if (!std::experimental::filesystem::exists("cfg/config.txt"))
	{
		getStream().open("cfg/config.txt", std::ios_base::out);
		getStream().close();
	}
}


void ConfigFile::writeLastName(std::string const & _name)
{
	open(std::ios_base::trunc | std::ios_base::out | std::ios_base::in, "cfg/config.txt");
	getStream() << "Last name:" << _name << ";";
	close();
}

std::string ConfigFile::getLastName()
{
	open(std::ios_base::out | std::ios_base::in, "cfg/config.txt");
	if (this->is_empty())
		writeLastName("Player");
	getStream().seekg(0, std::ios_base::beg);
	std::string currentLine;
	std::getline(getStream(), currentLine);
	std::string name;

	name = ExtractString(currentLine, ":", ";");

	getStream().seekg(0, std::ios_base::beg);
	close();
	return name;
}

ConfigFile::~ConfigFile()
{
	if (getStream().is_open())
		getStream().close();
}