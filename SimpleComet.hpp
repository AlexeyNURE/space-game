#pragma once
#pragma once

#include "Comet.hpp"
#include "CometKind.hpp"

class SimpleComet
	: public Comet
{
public:
	SimpleComet(POINT _point, double _k, int _b, int _speed);
	~SimpleComet() = default;
	int getDamage() override;
	CometKind getKind() const override;
};

inline CometKind SimpleComet::getKind() const
{
	return CometKind::Simple;
}