#ifndef _STAGE_FACTORY_
#define _STAGE_FACTORY_

class Stage;

class StageFactory
{
public:

	virtual Stage * nextStage(int _currentStage) const = 0;

	virtual ~StageFactory() = default;
};

#endif // !_STAGE_FACTORY_