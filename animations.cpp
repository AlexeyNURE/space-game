#include "animations.hpp"

void Animation::draw( Graphics & _graphics ) noexcept
{
	Point points[] = {
			{ m_coords.x,  m_coords.y }
		,	{ m_coords.x + m_imageSideLenght,  m_coords.y }
		,	{ m_coords.x,  m_coords.y + m_imageSideLenght }
	};
	int count = 3;

	int width = m_image->GetWidth() / m_framesAmount;

	_graphics.DrawImage(
			m_image
		,	points
		,	count
		,	width * m_currentFrame
		,	0
		,	width
		,	m_image->GetHeight()
		,	UnitPixel
	);

	++m_currentFrame;
}
