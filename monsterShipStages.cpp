#include "monsterShipStages.hpp"
#include "monsterShip.hpp"
#include "moveSide.hpp"
#include "Gameplay.hpp"
#include "Shot.hpp"
#include "boss.hpp"
#include <random>
#include <math.h>
#include <ctime>
#include <cassert>
#include <iostream>

extern Gameplay gameplay;
extern HWND Wnd;

/*************************************************************************/

MonsterShipStageDissappear::MonsterShipStageDissappear()
	:Stage(20, 150)
{
}

POINT MonsterShipStageDissappear::move(POINT _coords)
{
	POINT result = _coords;

	result.y -= m_moveSpeed;

	return result;
}

std::list< Shot * > MonsterShipStageDissappear::shoot(POINT _coords)
{
	static clock_t previous_shot = clock();
	clock_t current_shot = clock();

	std::list< Shot * > result;

	if (current_shot - previous_shot > m_shootDelay)
	{
		result.push_back(new Shot(gameplay.getBoss()->getCentreCoords(), gameplay.getBoss()->getAngle() + 180, 10));

		previous_shot = current_shot;
	}

	POINT ship = gameplay.getShip()->getCentreCoords();
	gameplay.getBoss()->rotateOn(ship);

	return result;
}

bool MonsterShipStageDissappear::isComplete() const noexcept
{
	return gameplay.getBoss()->getYCoord() < -1*gameplay.getBoss()->getBitmapSideLenght();
}

/*************************************************************************/

MonsterShipStageAppear::MonsterShipStageAppear()
	:Stage(8, 300)
{
}

POINT MonsterShipStageAppear::move(POINT _coords)
{
	RECT _rect;
	GetClientRect(Wnd, &_rect);
	POINT result = _coords;
	if (_coords.y <= -1*gameplay.getBoss()->getBitmapSideLenght())
	{
		std::random_device rd;  //Will be used to obtain a seed for the random number engine
		std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
		std::uniform_real_distribution<> dis(200, _rect.right - 200);
		int x = dis(gen);
		result.x = x;
	}

	POINT ship = gameplay.getShip()->getCentreCoords();
	gameplay.getBoss()->rotateOn(ship);

	result.y += m_moveSpeed;

	return result;
}

std::list< Shot * > MonsterShipStageAppear::shoot(POINT _coords)
{
	static clock_t previous_shot = clock();
	clock_t current_shot = clock();

	std::list< Shot * > result;

	if (current_shot - previous_shot > m_shootDelay)
	{
		result.push_back(new Shot(gameplay.getBoss()->getCentreCoords(), gameplay.getBoss()->getAngle() + 180, 10));

		previous_shot = current_shot;
	}

	return result;
}

bool MonsterShipStageAppear::isComplete() const noexcept
{
	return gameplay.getBoss()->getYCoord() > 100;
}

/*************************************************************************/

Stage * MonsterShipStageFactory::nextStage(int _currentStage) const
{
	switch (_currentStage)
	{
	case 0:
		return new MonsterShipStageAppear;
	case 1:
		return new MonsterShipStageDissappear;

	default:
		throw std::logic_error( "Invalid stage number" );
	}
	return nullptr;
}
