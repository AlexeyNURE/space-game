#include "alienShipStages.hpp"
#include "alienShip.hpp"
#include "moveSide.hpp"
#include "Gameplay.hpp"
#include "Shot.hpp"
#include "boss.hpp"

#include <ctime>
#include <cassert>
#include <iostream>

extern Gameplay gameplay;
extern HWND Wnd;

/*************************************************************************/

AppearStage::AppearStage()
	:Stage( 8, 0 )
{
}

POINT AppearStage::move( POINT _coords )
{
	POINT result = _coords;

	result.y += m_moveSpeed;

	return result;
}

std::list< Shot * > AppearStage::shoot( POINT _coords )
{
	return {};
}

bool AppearStage::isComplete() const noexcept
{
	return gameplay.getBoss()->getYCoord() > 50;
}

/*************************************************************************/

AlienShipStageOne::AlienShipStageOne()
	:Stage( 12, 300 )
{
}

POINT AlienShipStageOne::move( POINT _coords )
{
	static MoveSide side = MoveSide::RIGHT;

	RECT rect;
	GetClientRect( Wnd, &rect );

	return Stage::simpleMoves( _coords, rect, side, m_moveSpeed );
}

std::list< Shot * > AlienShipStageOne::shoot( POINT _coords )
{
	static clock_t previous_shot = clock();
	clock_t current_shot = clock();

	std::list< Shot * > result;

	if ( current_shot - previous_shot > m_shootDelay )
	{
		result.push_back( new Shot( gameplay.getBoss()->getCentreCoords(), 180, 10) );
		previous_shot = current_shot;
	}

	return result;
}

bool AlienShipStageOne::isComplete() const noexcept
{
	const Boss * boss = gameplay.getBoss();

	return boss->getMaximalHP() - boss->getCurrentHP()
		>= ( boss->getMaximalHP() / boss->getStagesCount() ) * boss->getCurrentStage();
}

/*************************************************************************/

AlienShipStageTwo::AlienShipStageTwo()
	:Stage( 8, 500 )
{
}

POINT AlienShipStageTwo::move( POINT _coords )
{
	static MoveSide side = MoveSide::RIGHT;

	RECT rect;
	GetClientRect( Wnd, &rect );

	return Stage::simpleMoves( _coords, rect, side, m_moveSpeed );
}

std::list< Shot * > AlienShipStageTwo::shoot( POINT _coords )
{
	static clock_t previous_shot = clock();
	clock_t current_shot = clock();

	std::list< Shot * > result;

	if ( current_shot - previous_shot > m_shootDelay )
	{
		for ( int i{ 120 }; i <= 240; i += m_shootStep )
			result.push_back( new Shot( gameplay.getBoss()->getCentreCoords(), i, 10 ) );

		previous_shot = current_shot;
	}

	return result;
}

bool AlienShipStageTwo::isComplete() const noexcept
{
	const Boss * boss = gameplay.getBoss();
	return boss->getMaximalHP() - boss->getCurrentHP()
		>= (boss->getMaximalHP() / boss->getStagesCount())*boss->getCurrentStage();
}

/*************************************************************************/

AlienShipStageThree::AlienShipStageThree()
	:Stage( 15, 200 )
{
}

POINT AlienShipStageThree::move( POINT _coords )
{
	POINT playerCentreCoords{ gameplay.getShip()->getCentreCoords() };
	POINT bossCentreCoords{ gameplay.getBoss()->getCentreCoords() };
	int diffCentreX{ playerCentreCoords.x - bossCentreCoords.x };
	int bossBitmapLenght{ gameplay.getBoss()->getBitmapSideLenght() };

	if ( abs( diffCentreX ) < m_moveSpeed )
		_coords.x = playerCentreCoords.x - bossBitmapLenght / 2;
	else if ( diffCentreX > 0 )
		_coords.x += m_moveSpeed;
	else
		_coords.x -= m_moveSpeed;

	// if the boss is getting out of screen it fixes here
	RECT rt;
	GetClientRect( Wnd, &rt );

	if ( _coords.x < rt.left )
		_coords.x = rt.left;

	if ( _coords.x > rt.right - bossBitmapLenght )
		_coords.x = rt.right - bossBitmapLenght;

	return _coords;
}

std::list< Shot * > AlienShipStageThree::shoot( POINT _coords )
{
	static clock_t previous_shot = clock();
	clock_t current_shot = clock();

	std::list< Shot * > result;

	if ( current_shot - previous_shot > m_shootDelay )
	{
		result.push_back( new Shot( gameplay.getBoss()->getCentreCoords(), 180, 10 ) );
		previous_shot = current_shot;
	}

	return result;
}

bool AlienShipStageThree::isComplete() const noexcept
{
	const Boss * boss = gameplay.getBoss();
	return boss->getMaximalHP() - boss->getCurrentHP()
		>= (boss->getMaximalHP() / boss->getStagesCount())*boss->getCurrentStage();
}

/*************************************************************************/

Stage * AlienShipStageFactory::nextStage( int _currentStage ) const
{
	switch ( _currentStage )
	{
		case 0:
			return new AppearStage;
		case 1:
			return new AlienShipStageOne;
		case 2:
			return new AlienShipStageTwo;
		case 3:
			return new AlienShipStageThree;

		default:
			throw std::logic_error( "Invalid stage number" );
	}
	return nullptr;
}

/*************************************************************************/