#include "monsterShip.hpp"
#include "resource.h"
#include "monsterShipStages.hpp"
#include <ctime>
#include <windows.h>

extern HINSTANCE hInst;

MonsterShip::~MonsterShip()
{
	delete m_stage;
	delete m_bossBitmap;
	delete m_stageFactory;
}

MonsterShip::MonsterShip(POINT _coords)
	:Boss(200, 2, _coords, 200)
{
	m_stageFactory = new MonsterShipStageFactory;
	m_stage = m_stageFactory->nextStage( m_currentStage );

	m_bossBitmap = new Gdiplus::Image(L"images/boss2.png");
	m_bossBitmap->RotateFlip( Gdiplus::RotateNoneFlipY );
}

void MonsterShip::checkStage() noexcept
{
	if (m_stage->isComplete())
	{
		delete m_stage;

		if (m_currentStage == 1)
			m_currentStage--;
		else
			m_currentStage++;

		m_stage = m_stageFactory->nextStage(m_currentStage);
	}
}