#include "sounds.hpp"

#pragma comment (lib,"lib/audiere.lib")

Sound::Sound()
{
	m_device = OpenDevice();
}

void Sound::play(std::string const &_name, bool is_thread)
{
	m_Sounds[_name] = OpenSound(m_device, _name.c_str(), is_thread);
	m_Sounds[_name]->play();
}

void Sound::stop(std::string const &_name)
{
	m_Sounds[_name]->stop();
}

void Sound::clear()
{
	m_Sounds.clear();
}

Sound::~Sound()
{
	for (auto it : m_Sounds)
		if (it.second->isPlaying())
			it.second->stop();
}