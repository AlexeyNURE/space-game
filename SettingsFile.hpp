#pragma once


#include "FileManager.hpp"

class Player;

class SettingsFile
	: public FileManager
{
private:
	using score = std::pair < std::string, int>; // first - name, second - points

	struct Comp
	{
		bool operator ()(const score &_l, const score &_r)
		{
			return _l.second == _r.second ? _l.first > _r.first : _l.second < _r.second;
		}
	};
	std::deque<score> m_LastResults;
	std::set<score, Comp> m_BestResults;

	static const int MAX_AMOUNT_RESULTS = 10;

	void init();
public:
	SettingsFile();
	~SettingsFile() override;
	void writeNewRecord();
	void writeLastResult(const Player &);
	std::pair<std::string, int> getTopResult() const;
	std::pair<std::string, int> getLastResult() const;
	void forEachLastResults(std::function<void(std::pair < std::string, int> const &)> _action) const;
	void forEachTopResults(std::function<void(std::pair < std::string, int> const &)> _action) const;

};