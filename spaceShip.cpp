#include "SpaceShip.hpp"
#include "resource.h"
#include "wave.hpp"
#include "Shot.hpp"

#include <Windows.h>
#include <iostream>
#include <gdiplus.h>

#pragma comment(lib, "Gdiplus.lib")

extern HWND Wnd;
extern HINSTANCE hInst;

SpaceShip::SpaceShip()
	: RotatbleObject( 100 )
{
	m_currentHP = m_maxHP;
	m_lastTimeHit = 0;

	m_coords = {};

	resetMoving();

	m_image = new Gdiplus::Image( L"images/ship.png" );
	m_image->RotateFlip(Gdiplus::Rotate180FlipNone);

	m_imageWhenHit = new Gdiplus::Image( L"images/shipHit.png" );
	m_imageWhenHit->RotateFlip( Gdiplus::Rotate180FlipNone );
}

SpaceShip::~SpaceShip()
{
	delete m_image;
	delete m_imageWhenHit;

	resetMoving();
}

void SpaceShip::move( RECT _rect ) noexcept
{
	POINT previous_state = m_coords;

	// checks for horizontal moves

	if ( move_left )
		m_coords.x -= m_moveSpeed;

	if ( move_right )
		m_coords.x += m_moveSpeed;

	if ( !isValidOnScreen( _rect ) )
		m_coords = previous_state;
	else
		previous_state = m_coords;

	//------------------------------

	// checks for vertical moves

	if ( move_up )
		m_coords.y -= m_moveSpeed;

	if ( move_down )
		m_coords.y += m_moveSpeed;

	if (!isValidOnScreen(_rect))
		m_coords = previous_state;
}


Shot * SpaceShip::createShot() const noexcept
{
	Shot * pShot = new Shot( getCentreCoords(), getAngle(), m_ShotDamage);

	return pShot;
}

void SpaceShip::setCoordinates( POINT _coords ) noexcept
{
	m_coords = _coords;
}

void SpaceShip::takeDamage( int _damage ) noexcept
{
	m_lastTimeHit = clock();
	m_currentHP -= _damage;
}

void SpaceShip::resetMoving() noexcept
{
	move_down = false;
	move_left = false;
	move_right = false;
	move_up = false;

	rotate_left = false;
	rotate_right = false;
}

Wave * SpaceShip::createWave() const noexcept
{
	POINT shipCentreCoords = getCentreCoords();

	Wave * pWave = new Wave( shipCentreCoords );

	return pWave;
}

void SpaceShip::update( RECT _rect ) noexcept
{
	move( _rect );

	rotate();
}
