#ifndef _DRAW_
#define _DRAW_

#include <Windows.h>

void  DrawBitmap(HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
{
	BITMAP bm;
	HDC hdcMem;
	POINT ptSize, ptOrg;
	hdcMem = CreateCompatibleDC(hdc);
	SelectObject(hdcMem, hBitmap);
	SetMapMode(hdcMem, GetMapMode(hdc));
	GetObject(hBitmap, sizeof(BITMAP), (LPVOID)&bm);
	ptSize.x = bm.bmWidth;
	ptSize.y = bm.bmHeight;
	DPtoLP(hdc, &ptSize, 1);
	ptOrg.x = 0;
	ptOrg.y = 0;
	DPtoLP(hdcMem, &ptOrg, 1);
	GdiTransparentBlt(
		hdc, xStart, yStart, ptSize.x, ptSize.y,
		hdcMem, 0, 0, ptSize.x, ptSize.y, RGB(255, 255, 255)
	);
	DeleteObject(&bm);
	DeleteDC(hdcMem);
}

#endif // !_DRAW_