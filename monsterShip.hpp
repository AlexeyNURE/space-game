#ifndef _MONSTERSHIP_HPP_
#define _MONSTERSHIP_HPP_

#include "rotatbleObject.hpp"
#include "boss.hpp"
#include "Shot.hpp"

#include <list>

class MonsterShip : public Boss
{
public:

	MonsterShip() = delete;

	~MonsterShip();

	MonsterShip(POINT _coords);

	void checkStage() noexcept override;

	MonsterShip(const MonsterShip &) = delete;

	MonsterShip & operator = (const MonsterShip &) = delete;
};

#endif