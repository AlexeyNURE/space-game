#include "Shot.hpp"

#define _USE_MATH_DEFINES // for C  
#include <math.h>

Shot::Shot(POINT _coords, int _angle, int _damage)
	:FlyingObject( 15 )
{
	m_Damage = _damage;
	m_angle = _angle;
	m_coords = _coords;
}

void Shot::move() noexcept
{
	m_coords.x += m_moveSpeed * sin(m_angle * M_PI / 180);
	m_coords.y -= m_moveSpeed * cos(m_angle* M_PI / 180);
}
